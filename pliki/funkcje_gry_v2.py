"""Funkcje pomocnicze do gry w zgadywanie słów.
"""

import requests
from random import randint


def tworz_slownik():
    """Wczytanie i wyczyszczenie listy polskich słów, ale bez znaków diakrytycznych.
    Zwraca słownik z listą słów.
    
    Dane pochodzą z repozytorium https://github.com/lchsk/polish-dictionary
    """
    
    slownik = 'https://raw.githubusercontent.com/lchsk/polish-dictionary/master/dictionary/pl_infl_ascii'
    slowa = requests.get(slownik)
    slowa = slowa.text
    slowa = slowa.split('\n')
    lista_slow = []
    for slowo in slowa:
        lista_slow.append(slowo[:slowo.find("=")])
    ile_slow = len(lista_slow)
    
    return lista_slow


def losuj_slowo(lista_slow):
    """Zwraca jeden losowy wyraz z podanej listy.
    """
    
    wylosowane = randint(1,len(lista_slow))
    wyraz = lista_slow[wylosowane]
    
    return wyraz


print('Uruchamia się przy imporcie.')


if __name__ == '__main__':
    print('Uruchomione jako skrypt')
    print('Nie uruchamia się przy imporcie.')
    
    slowa = tworz_slownik()
    for i in range(3):
        print(i, losuj_slowo(slowa))
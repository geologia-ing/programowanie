import requests
from random import randint


def tworz_slownik():
    slownik = 'https://raw.githubusercontent.com/lchsk/polish-dictionary/master/dictionary/pl_infl_ascii'
    slowa = requests.get(slownik)
    slowa = slowa.text
    slowa = slowa.split('\n')
    lista_slow = []
    for slowo in slowa:
        lista_slow.append(slowo[:slowo.find("=")])
    ile_slow = len(lista_slow)
    
    return lista_slow


def losuj_slowo(lista_slow):
    wylosowane = randint(1,len(lista_slow))
    wyraz = lista_slow[wylosowane]
    
    return wyraz